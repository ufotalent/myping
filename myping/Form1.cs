﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading;

namespace myping
{
    
    public partial class Form1 : Form
    {
        public static string add = "www.cc98.org";
        public Form1()
        {
            InitializeComponent();
        }
        [DllImport("user32.dll")]
        public static extern
            Int32 GetWindowLong(IntPtr hwnd, Int32 index);
        [DllImport("user32.dll")]
        public static extern
            Int32 SetWindowLong(IntPtr hwnd, Int32 index, Int32 newValue);
        public const int GWL_EXSTYLE = -20;
        public static void AddWindowExStyle(IntPtr hwnd, Int32 val)
        {
            int oldValue = GetWindowLong(hwnd, GWL_EXSTYLE);
            if (oldValue == 0)
            {
                throw new System.ComponentModel.Win32Exception();
            }
            if (0 == SetWindowLong(hwnd, GWL_EXSTYLE, oldValue | val))
            {
                throw new System.ComponentModel.Win32Exception();
            }
        }
        public static int WS_EX_TOOLWINDOW = 0x00000080;
        //我把这个过程封装下： 
        public static void SetFormToolWindowStyle(System.Windows.Forms.Form form)
        {
            AddWindowExStyle(form.Handle, WS_EX_TOOLWINDOW);
        }
        public static int left, top;
        int[] Time = new int[50];
        bool[] Suc = new bool[50];
        string[] info = new string[50];

        bool closed = false;
        const int timeout = 1000;
        System.Threading.Thread PingThread;
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Width = 50;
            this.Height = 50;
            this.ShowInTaskbar = false;
            SetFormToolWindowStyle(this);
            for (int i = 0; i < 50; i++) {
                Time[i] = 0;
                Suc[i] = true;
                info[i] = "";
            }
            this.TopMost = true;
            PingThread=new System.Threading.Thread(ping);
            PingThread.Start();
            tooltip.ShowAlways = true;
            this.Top = top;
            this.Left = left;
        }

        public void ping() {
            while (true)
            {
                if (closed)
                {
                    return;
                }
                Ping ping = new Ping();


                try
                {

                    for (int i = 48; i >= 0; i--)
                    {
                        Time[i + 1] = Time[i];
                        Suc[i + 1] = Suc[i];
                        info[i + 1] = info[i];
                    }
                    PingReply ret = ping.Send(add, timeout);
                    Time[0] = (int)ret.RoundtripTime;
                    if (ret.Status == IPStatus.Success)
                        Suc[0] = true;
                    else
                    {
                        Suc[0] = false;
                        info[0] = ret.Status.ToString();
                    }


                }
                catch (System.Exception e)
                {
                    Suc[0] = false;
                    info[0] = "can not solve IP";
                    //MessageBox.Show(e.ToString());
                }
                finally
                {
                    Thread.Sleep(timeout - Time[0]);
                }
                
            }

        }
        ToolTip tooltip = new ToolTip();
        private void timer1_Tick(object sender, EventArgs e)
        {
           
          

            //MessageBox.Show(ret.RoundtripTime.ToString());
            Graphics g = this.CreateGraphics();
            //g.Clear(this.BackColor);
            Pen p = new Pen(Color.Red);
            Pen fatal = new Pen(Color.Blue);
            Pen back = new Pen(this.BackColor);
            for (int i = 0; i < 50; i++)
            {
                
                if (Suc[i])
                {
                    g.DrawLine(back, new Point(i, 0), new Point(i, 50 - (int)(Math.Sqrt(Time[i]) * 50 / Math.Sqrt(timeout))));
                    g.DrawLine(p, new Point(i, 50 - (int)(Math.Sqrt(Time[i]) * 50 / Math.Sqrt(timeout))), new Point(i, 50));
                }
                else
                {
                    if (info[i] == "can not solve IP")
                        g.DrawLine(fatal, new Point(i, 0), new Point(i, 50));
                    else
                        g.DrawLine(p, new Point(i, 0), new Point(i, 50));
                }
            }
        }

        private void Form1_Scroll(object sender, ScrollEventArgs e)
        {

            //MessageBox.Show("Sc");
        }
        bool Drag = false;
        int rx, ry;
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.contextMenuStrip1.Show(Control.MousePosition);
            }
            
            rx = Control.MousePosition.X-this.Left;
            ry = Control.MousePosition.Y - this.Top;
            Drag = true;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            Drag = false;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (Drag)
            {
                this.Left = Control.MousePosition.X - rx;
                this.Top = Control.MousePosition.Y - ry;
            }
            else 
            {
                if (e.X < 50 && e.X >= 0)
                {
              
                    if (Suc[e.X])
                        tooltip.Show(Time[e.X].ToString()+" ms",this, e.X + 10, e.Y+3);
                    else
                        tooltip.Show(info[e.X], this, e.X + 10, e.Y+3);
                }
                
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_MouseLeave(object sender, EventArgs e)
        {
            
            tooltip.Hide(this);
            PingThread.Resume();
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            PingThread.Suspend();
        }

        private void changeDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tmp=Microsoft.VisualBasic.Interaction.InputBox("new destination", "", add);
            if (tmp != "")
                add = tmp;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            closed = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

