﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace myping
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static int strToInt(String s)
        {
            int ret = 0;
            for (int j = 0; j < s.Length; j++)
                ret = ret * 10 + s[j] - '0';
            return ret;
        }
        [STAThread]

        static void Main(String[] args)
        {
            if (args.Length == 0)
                Form1.add = "localhost";
            else
            {
                Form1.add = args[0];
                Form1.left = strToInt(args[1]);
                Form1.top = strToInt(args[2]);

            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
